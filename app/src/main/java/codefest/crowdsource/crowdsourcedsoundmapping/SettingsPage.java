package codefest.crowdsource.crowdsourcedsoundmapping;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import java.util.ArrayList;
import java.util.List;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import codefest.crowdsource.crowdsourcedsoundmapping.data.Settings;

public class SettingsPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_page);

        // Allows the user to give permission for their data to be stored.
        final CheckBox collectionCheckbox = (CheckBox) findViewById(R.id.Collection_Checkbox);
        final CheckBox locationCheckbox = (CheckBox) findViewById(R.id.Location_Checkbox);

        // Checks to see if the checkboxes have been checked. If so it sets the settings for data retrieval to true.
        collectionCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(collectionCheckbox.isChecked())
                    Settings.allowCollection = true;
                else
                    Settings.allowCollection = false;
            }
        });

        collectionCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(locationCheckbox.isChecked())
                    Settings.allowMonitorLocation = true;
                else
                    Settings.allowMonitorLocation = false;
            }
        });

        // Sets up the continue button.
        Button aboutBtn = (Button) findViewById(R.id.About_Btn);
        aboutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchAboutPageActivity();
            }
        });

        // Spinner for changing the collection timing value.
        Spinner spinner = (Spinner) findViewById(R.id.Timing_Spinner);
        // The intervals that are available (in seconds).
        List<Integer> timingIntervals = new ArrayList<Integer>();
        timingIntervals.add(5);// Integer values for time intervals.
        timingIntervals.add(10);
        timingIntervals.add(15);
        timingIntervals.add(30);
        timingIntervals.add(60);

        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, timingIntervals);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        // Creates a listener that waits for a value to be selected, then saves that value as the time interval
        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Settings.collectionTiming = (int) parent.getSelectedItem();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Settings.collectionTiming = 15;
            }
        });

        // Noise level radio group
        RadioGroup noiseLevelSelector = (RadioGroup)findViewById(R.id.Noise_Levels);

        noiseLevelSelector.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup noiseLevels, int checkedId) {
                RadioButton rb = (RadioButton) noiseLevels.findViewById(checkedId);
                Settings.noiseDesired = rb.getTag().toString();
            }
        });

        // Allows the user to give permission for their data to be stored.
        final CheckBox dayNightCheckbox = (CheckBox) findViewById(R.id.Sleep_Cycle_Checkbox);

        dayNightCheckbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dayNightCheckbox.isChecked())
                    Settings.awakeDay = true;
                else
                    Settings.awakeDay = false;
            }
        });
    }

    // Launches an about page when called.
    public void LaunchAboutPageActivity()
    {
        Intent i = new Intent(this, AboutPage.class);
        startActivity(i);
    }
}

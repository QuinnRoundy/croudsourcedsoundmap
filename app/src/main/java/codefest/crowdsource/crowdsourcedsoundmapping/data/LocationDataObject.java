package codefest.crowdsource.crowdsourcedsoundmapping.data;

/**
 * Created by smajors on 4/7/17.
 */
public class LocationDataObject {
    private double decibelValue;        // Decibel value
    private double latitude;            // Latitude value
    private double longitude;           // Longitude value
    private double altitude;
    private boolean granularity;      // Was the reading fine data, or not
    private long timestamp;           // Timestamp of reading

    /**
     * Constructs a new LocationDataObject from numerical values
     * @param decibelValue The decibel reading of the point of reading
     * @param latitude  The latitude of the point of reading
     * @param longitude The longitude of the point of reading
     * @param granularity Is the reading fine or not granular
     * @param timestamp Timestamp of the reading
     * @return A new constructed LocationDataObject from numerical values
     */
    public static LocationDataObject getDataObject(double decibelValue, double latitude, double longitude, double altitude, boolean granularity, long timestamp)
        {
        return new LocationDataObject(decibelValue, latitude, longitude, altitude, granularity, timestamp);
    }
    /**
     * Constructs a new LocationDataObject from strings
     * @param decibelValue The decibel reading of the point of reading
     * @param latitude  The latitude of the point of reading
     * @param longitude The longitude of the point of reading
     * @param granularity Is the reading fine or not granular
     * @param timestamp Timestamp of the reading
     * @return A new constructed LocationDataObject from string values
     */
    public static LocationDataObject getDataObject(String decibelValue, String latitude, String longitude, String altitude, boolean granularity, String timestamp) {
        double db, lat, lon, alt;
        long time;

        try {
            db = Double.parseDouble(decibelValue);
            lat = Double.parseDouble(latitude);
            lon = Double.parseDouble(longitude);
            time = Long.parseLong(timestamp);
            alt = Double.parseDouble(altitude);
        } catch (NumberFormatException nfe) {
            throw new NumberFormatException("LocationDataObject was unable to parse a value.\n" + nfe.getMessage());
        }

        return new LocationDataObject(db, lat, lon, alt, granularity, time);
    }

    LocationDataObject(double decibelValue, double latitude, double longitude, double altitude, boolean granularity, long timestamp) {
        this.decibelValue = decibelValue;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.granularity = granularity;
        this.timestamp = timestamp;
    }

    public double getDecibelValue() {
        return decibelValue;
    }

    public void setDecibelValue(double decibelValue) {
        this.decibelValue = decibelValue;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isGranularity() {
        return granularity;
    }

    public void setGranularity(boolean granularity) {
        this.granularity = granularity;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return String.format("{%f,%f,%f,%s,%d}", this.getDecibelValue(), this.getLatitude(), this.getLongitude(), this.isGranularity(), this.getTimestamp());

    }

}

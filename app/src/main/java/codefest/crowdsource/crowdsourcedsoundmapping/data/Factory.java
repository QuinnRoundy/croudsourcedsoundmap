package codefest.crowdsource.crowdsourcedsoundmapping.data;

import android.app.AlarmManager;
import android.content.Context;

import codefest.crowdsource.crowdsourcedsoundmapping.AlarmReceiver;

/**
 * Created by Azrhei on 4/8/2017.
 */

public class Factory {
    private Factory() {}

    public static Context applicationContext = getApplicationContext();
    public static LocationReader location = new LocationReader(applicationContext);

    private static Context getApplicationContext() {
        return applicationContext;
    }
    public static SoundReader sound = new SoundReader();
}

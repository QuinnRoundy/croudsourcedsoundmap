package codefest.crowdsource.crowdsourcedsoundmapping.data;

/**
 * Created by chrys on 4/8/2017.
 */

public class Coordinate {
    private double latitude;
    private double longitude;
    private double altitude;

    public Coordinate(double latitude, double longitude, double altitude){
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    // Immutable......setters not allowed
//    public void setCoordinates(double latitude, double longitude){
//        this.latitude = latitude;
//        this.longitude = longitude;
//    }
    public double getLatitude(){
        return latitude;
    }
    public double getLongitude(){
        return longitude;
    }
    public double getAltitude() { return altitude; }
}

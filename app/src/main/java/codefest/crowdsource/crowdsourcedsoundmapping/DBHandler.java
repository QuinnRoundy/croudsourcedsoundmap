
package codefest.crowdsource.crowdsourcedsoundmapping;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

import codefest.crowdsource.crowdsourcedsoundmapping.dbo.DatabaseHelper;
import codefest.crowdsource.crowdsourcedsoundmapping.types.DataBlob;

/**
 * Created by chrys on 4/7/2017.
 */

public class DBHandler {
    private HashMap<String,Double> data = new HashMap<>();
    private Context context = null;
    DBHandler(Context context){
        /*double decibels = 0.0;
        Coordinate x = new Coordinate(0,0);
        data.put("decibels",decibels);
        data.put("coordinate",x);*/
        this.context = context;
    }

    private void pushData(String data, String label){
        int q = data.indexOf("\""+label+"\"");

        String part1 = data.substring(q+1);
        Log.i("TEST","part1: " + part1);
        String part1_2 = part1.substring(0,part1.indexOf(":")-1);
        Log.i("TEST","part1_2: " + part1_2);
        String part2_0 = part1.substring(part1.indexOf(":")+2);
        String part2 = part2_0.substring(0, part2_0.indexOf("\""));
        Log.i("TEST","part2: " + part2);
        double obj = Double.parseDouble(part2);
        this.data.put(label, obj);
        Log.i("Test", "q: " + this.data.get(label));
    }

    public void importData(String data){
        pushData(data,"Decibels");
        pushData(data,"Longitude");
        pushData(data,"Latitude");
        pushData(data,"Altitude");
        pushData(data,"Granularity");
        pushData(data,"Timestamp");
        //pushData(data,"somethingElse");
        //pushData(data,"somethingElse");
    }
    public void importData(DataBlob data){
        this.data.put("Decibels",data.getAmplitude());
        this.data.put("Latitude",data.getLatitude());
        this.data.put("Longitude",data.getLongitude());
        this.data.put("Altitude",data.getAltitude());
        this.data.put("Granularity",data.getGranularity());
        this.data.put("Timestamp",data.getDatetimestamp());
    }
    public void commitData(){
        String tableName = "cssma_test";
        String existsQuery = "SELECT * FROM sqlite_master WHERE name = '"+tableName+"' and type='table';";
        //SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase("CSSMA", null);
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(":memory:",null);
        Cursor cursor = db.rawQuery(existsQuery,null);
        int entries = cursor.getCount();
        DatabaseHelper dbh = new DatabaseHelper(context);
        dbh.insertIntoCSSMATest(data);
        SQLiteDatabase d2b = SQLiteDatabase.openOrCreateDatabase("/data/user/0/codefest.crowdsource.crowdsourcedsoundmapping/databases/CSSMA", null);
        Cursor cursor2 = d2b.rawQuery("SELECT * FROM CSSMA_TEST;",null);
        int entries2 = cursor2.getCount();
        Log.i("DBOPS","NewEntries "+entries2);
        Log.i("DBOPS",""+entries);
        //ArrayList<HashMap<String,Double>> data = dbh.getFromCSSMATest();
        //Log.i("DBOPS","TESTOFFAITH: " + data.get(0).get("Decibel"));
        if (cursor2.getPosition() < 0) cursor2.moveToNext();
        if(entries2 > 0){
            Log.i("DATATEST",cursor2.getString(0));
            Log.i("DATATEST",cursor2.getString(1));
            Log.i("DATATEST",cursor2.getString(2));
            Log.i("DATATEST",cursor2.getString(3));
            Log.i("DATATEST",cursor2.getString(4));
            Log.i("DATATEST",cursor2.getString(5));
            Log.i("DATATEST",cursor2.getString(6));
            cursor2.getPosition();

        }
    }
    void getVersion() {
        String query = "select sqlite_version() AS sqlite_version";
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(":memory:", null);
        Cursor cursor = db.rawQuery(query, null);
        String sqliteVersion = "";
        if (cursor.moveToNext()) {
            sqliteVersion = cursor.getString(0);
            Log.i("Test","Version: " + sqliteVersion);
        }
    }
}

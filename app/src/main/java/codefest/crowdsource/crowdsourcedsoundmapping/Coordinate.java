package codefest.crowdsource.crowdsourcedsoundmapping;

/**
 * Created by chrys on 4/8/2017.
 */

public class Coordinate {
    private double latitude;
    private double longitude;
    public Coordinate(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public void setCoordinates(double latitude, double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }
    public double getLatitude(){
        return latitude;
    }
    public double getLongitude(){
        return longitude;
    }
}

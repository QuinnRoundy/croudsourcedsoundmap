package codefest.crowdsource.crowdsourcedsoundmapping;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioButton;
import android.view.View;
import android.view.View.OnClickListener;

import codefest.crowdsource.crowdsourcedsoundmapping.data.Settings;

public class TermsAndConditionsPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_and_conditions_page);

        RadioButton acceptTermsBtn = (RadioButton) findViewById(R.id.Terms_And_Cond_Butt);
        acceptTermsBtn.setOnClickListener(new OnClickListener (){
            public void onClick(View v) {
                Settings.allowCollection = true;
                Settings.allowMonitorLocation = true;
            }
        });
    }
}

package codefest.crowdsource.crowdsourcedsoundmapping;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import codefest.crowdsource.crowdsourcedsoundmapping.data.Factory;
import codefest.crowdsource.crowdsourcedsoundmapping.data.LocationReader;
import codefest.crowdsource.crowdsourcedsoundmapping.data.SoundReader;

public class LocationActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_getAltitude, btn_getLatitude, btn_getLongitude;
    EditText tv_altitude, tv_latitude, tv_longitude;
    LocationReader location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        btn_getAltitude = (Button) findViewById(R.id.btn_getAltitude);
        btn_getLatitude = (Button) findViewById(R.id.btn_getLatitude);
        btn_getLongitude = (Button) findViewById(R.id.btn_getLongitude);

        tv_altitude = (EditText) findViewById(R.id.tv_Altitude);
        tv_latitude = (EditText) findViewById(R.id.tv_Latitude);
        tv_longitude = (EditText) findViewById(R.id.tv_Longitude);

        btn_getAltitude.setOnClickListener(this);
        btn_getLatitude.setOnClickListener(this);
        btn_getLongitude.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch(v.getId())
        {
            case R.id.btn_getAltitude:
                tv_altitude.setText(Double.toString(Factory.location.getAltitude()));
                break;
            case R.id.btn_getLatitude:
                tv_latitude.setText(Double.toString(Factory.location.getLatitude()));
                break;
            case R.id.btn_getLongitude:
                tv_longitude.setText(Double.toString(Factory.location.getLongitude()));
                break;
        }
    }
}

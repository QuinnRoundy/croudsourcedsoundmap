package codefest.crowdsource.crowdsourcedsoundmapping;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;

import codefest.crowdsource.crowdsourcedsoundmapping.data.Factory;
import codefest.crowdsource.crowdsourcedsoundmapping.data.SoundReader;

public class SoundActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btn_getAmplitude;
    private TextView tv_amplitude;
    private ProgressBar pb_amplitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound);

        btn_getAmplitude = (Button) findViewById(R.id.btn_getAmplitude);
        btn_getAmplitude.setOnClickListener(this);

        tv_amplitude = (TextView) findViewById(R.id.tv_amplitude);

        pb_amplitude = (ProgressBar) findViewById(R.id.pb_amplitude);
    }

    @Override
    public void onClick(View v) {

        if(Factory.sound != null) {
            Factory.sound.start();
        }
        else {
            Factory.sound = new SoundReader();
            Factory.sound.start();
        }

            switch (v.getId()) {
                case R.id.btn_getAmplitude:
                    double amp = Factory.sound.getDecibels();

                    tv_amplitude.setText(Double.toString(amp));
                    pb_amplitude.setProgress((int) amp);

                    break;
            }
            Factory.sound.stop();
        }


}

package codefest.crowdsource.crowdsourcedsoundmapping.types;

/**
 * Created by chrys on 4/8/2017.
 */

public class DataBlob {
    private double amplitude;
    private double latitude;
    private double longitude;
    private double altitude;
    private double granularity;
    private double datetimestamp;

    public DataBlob(double amplitude, double latitude, double longitude, double altitude, double granularity, double datetimestamp) {
        this.amplitude = amplitude;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.granularity = granularity;
        this.datetimestamp = datetimestamp;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public void setAmplitude(double amplitude) {
        this.amplitude = amplitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getGranularity() {
        return granularity;
    }

    public void setGranularity(double granularity) {
        this.granularity = granularity;
    }

    public double getDatetimestamp() {
        return datetimestamp;
    }

    public void setDatetimestamp(double datetimestamp) {
        this.datetimestamp = datetimestamp;
    }
}

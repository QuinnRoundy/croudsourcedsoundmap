package codefest.crowdsource.crowdsourcedsoundmapping;

import android.content.Intent;
import android.widget.Button;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import codefest.crowdsource.crowdsourcedsoundmapping.data.Settings;

public class Welcome_Page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome__page);

        // Sets up the read more button.
        Button readMoreBtn = (Button) findViewById(R.id.AboutPage);
        readMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchAboutPageActivity();
            }
        });

        // Sets up the checkbox.
        final CheckBox consentCheckbox = (CheckBox) findViewById(R.id.Consent_Checkbox);

        // Sets up the continue button.
        Button continueBtn = (Button) findViewById(R.id.Continue_Btn);
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // If the checkbox was checked, then it sets the allowance settings to true and goes to the main activity.
                if(consentCheckbox.isChecked())
                {
                    Settings.allowCollection = true;
                    Settings.allowMonitorLocation = true;
                }
                LaunchMainActivity();
            }
        });
    }

    // Launches an about page when called.
    public void LaunchAboutPageActivity()
    {
        Intent i = new Intent(this, AboutPage.class);
        startActivity(i);
    }

    //Goes to the main function (map screen) when called
    public void LaunchMainActivity()
    {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
